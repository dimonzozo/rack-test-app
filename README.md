# Overview

Blank rack application. Can be used as template for hiring interviews.

# Running application

```
bundle exec shotgun config.ru
```

# Running tests

```
bundle exec rspec
```