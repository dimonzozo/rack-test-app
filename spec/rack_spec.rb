require_relative '../lib/application.rb'

RSpec.describe Application do
    context "get to /ruby/monstas" do
      let(:app)      { Application.new }
      let(:env)      { { "REQUEST_METHOD" => "GET", "PATH_INFO" => "/" } }
      let(:response) { app.call(env) }
      let(:status)   { response[0] }
      let(:body)     { response[2][0] }
  
      it "returns the status 200" do
        expect(status).to eq 200
      end
  
      it "returns the body" do
        expect(body).to eq "Hello world!"
      end
    end
  end